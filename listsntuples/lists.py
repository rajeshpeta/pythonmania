list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

print("original list is ", list)

list[3] = 77

# so lists are mutable we can easily change/modify the items inside the list 

print("Modified list is ", list)