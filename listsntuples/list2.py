# tuples are stored in single memory block
# lists are stored in two memory blocks

import sys
a_list = []
a_tuple = ()
a_list = ["geek", "for", "geeks"]
a_tuple = ("geek", "for", "geeks")

print(sys.getsizeof(a_list))
print(sys.getsizeof(a_tuple))